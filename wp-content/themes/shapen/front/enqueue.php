<?php
  function shapen_enqueue_scripts(){
    $url = get_theme_file_uri();
    $ver = SHAPEN_DEV_MODE ? time() : false;

    //CSS
    wp_register_style('bootstrap_css', $url . '/assets/css/bootstrap.min.css', [], false );
    wp_register_style('fontawesome_css', $url . '/assets/css/fontawesome/css/font-awesome.min.css', [], false );
    wp_register_style('owl_css', $url . '/assets/css/owl.carousel.min.css', [], false );
    wp_register_style('mag_css', $url . '/assets/css/magnific-popup.min.css', [], false );
    wp_register_style('load_css', $url . '/assets/css/loader.min.css', [], false );
    wp_register_style('flat_css', $url . '/assets/css/flaticon.min.css', [], false );
    wp_register_style('style_css', $url . '/assets/css/style.css', [], false );
    wp_register_style('skin_css', $url . '/assets/css/skin/skin-1.css', [], false );
    wp_register_style('switcher_css', $url . '/assets/css/switcher.css', [], false );
    wp_register_style('set_css', $url . '/assets/plugins/revolution/revolution/css/settings.css', [], false );
    wp_register_style('nav_css', $url . '/assets/plugins/revolution/revolution/css/navigation.css', [], false );
    wp_register_style('g_css', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', [], false );
    wp_register_style('g2_css', 'https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i', [], false );
    wp_register_style('custom_css', $url . '/style.css', [], false );
    wp_enqueue_style('bootstrap_css');
    wp_enqueue_style('fontawesome_css');
    wp_enqueue_style('owl_css');
    wp_enqueue_style('mag_css');
    wp_enqueue_style('load_css');
    wp_enqueue_style('flat_css');
    wp_enqueue_style('style_css');
    wp_enqueue_style('skin_css');
    wp_enqueue_style('switcher_css');
    wp_enqueue_style('set_css');
    wp_enqueue_style('nav_css');
    wp_enqueue_style('g_css');
    wp_enqueue_style('g2_css');
    wp_enqueue_style('custom_css');

    // JS
    wp_register_script('bootstrap_js', $url . '/assets/js/bootstrap.min.js', [], $ver, true);
    wp_register_script('mag_js', $url . '/assets/js/magnific-popup.min.js', [], $ver, true);
    wp_register_script('way_js', $url . '/assets/js/waypoints.min.js', [], $ver, true);
    wp_register_script('couter_js', $url . '/assets/js/counterup.min.js', [], $ver, true);
    wp_register_script('stick_js', $url . '/assets/js/waypoints-sticky.min.js', [], $ver, true);
    wp_register_script('pkgd_js', $url . '/assets/js/isotope.pkgd.min.js', [], $ver, true);
    wp_register_script('owl_js', $url . '/assets/js/owl.carousel.min.js', [], $ver, true);
    wp_register_script('owl2_js', $url . '/assets/js/jquery.owl-filter.js', [], $ver, true);
    wp_register_script('stellar_js', $url . '/assets/js/stellar.min.js', [], $ver, true);
    wp_register_script('custom_js', $url . '/assets/js/custom.js', [], $ver, true);
    wp_register_script('shorcode_js', $url . '/assets/js/shortcode.js', [], $ver, true);
    wp_register_script('bgscrol_js', $url . '/assets/js/jquery.bgscroll.js', [], $ver, true);
    wp_register_script('switch_js', $url . '/assets/js/switcher.js', [], $ver, true);
    wp_register_script('theme_js', $url . '/assets/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js', [], $ver, true);
    wp_register_script('punch_js', $url . '/assets/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js', [], $ver, true);
    wp_register_script('revo_js', $url . '/assets/plugins/revolution/revolution/js/extensions/revolution-plugin.js', [], $ver, true);
    wp_register_script('script_js', $url . '/assets/js/rev-script-1.js', [], $ver, true);
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap_js');
    wp_enqueue_script('mag_js');
    wp_enqueue_script('way_js');
    wp_enqueue_script('couter_js');
    wp_enqueue_script('stick_js');
    wp_enqueue_script('pkgd_js');
    wp_enqueue_script('owl_js');
    wp_enqueue_script('owl2_js');
    wp_enqueue_script('stellar_js');
    wp_enqueue_script('custom_js');
    wp_enqueue_script('shorcode_js');
    wp_enqueue_script('bgscrol_js');
    wp_enqueue_script('switch_js');
    wp_enqueue_script('theme_js');
    wp_enqueue_script('punch_js');
    wp_enqueue_script('revo_js');
    wp_enqueue_script('script_js');

  }
