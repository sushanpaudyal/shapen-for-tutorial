<?php
/**
 * Shapen Theme functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Shapen
 */
 ?>

 <?php
// Setup
define('SHAPEN_DEV_MODE', true);


// Includes Files
include(get_theme_file_path('/front/enqueue.php'));
include(get_theme_file_path('/front/setup.php'));


// Hooks
  add_action('wp_enqueue_scripts', 'shapen_enqueue_scripts');
