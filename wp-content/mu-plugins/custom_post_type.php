<?php
add_action('init', 'shapen_slider_post_type', 0 );


function shapen_slider_post_type() {
    // Labels for the Post Type
    $labels = array(
        'name'                => _x( 'Sliders', 'Post Type General Name', 'shapen' ),
        'singular_name'       => _x( 'Slider', 'Post Type Singular Name', 'shapen' ),
        'menu_name'           => __( 'Sliders', 'shapen' ),
        'parent_item_colon'   => __( 'Parent Slider', 'shapen' ),
        'all_items'           => __( 'All Sliders', 'shapen' ),
        'view_item'           => __( 'View Slider', 'shapen' ),
        'add_new_item'        => __( 'Add New Slider', 'shapen' ),
        'add_new'             => __( 'Add New Slider', 'shapen' ),
        'edit_item'           => __( 'Edit Slider', 'shapen' ),
        'update_item'         => __( 'Update Slider', 'shapen' ),
        'search_items'        => __( 'Search Slider', 'shapen' ),
        'not_found'           => __( 'No sliders found', 'shapen' ),
        'not_found_in_trash'  => __( 'Not found in trash', 'shapen' ),
    );
    // Another Customizations
    $args = array(
        'label'   => __('Sliders','shapen' ),
        'description' => __('Sliders for Shapen', 'shapen'),
        'labels'  => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menus' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 15,
        'menu_icon' => 'dashicons-images-alt',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
    );
    // register the post Type
    register_post_type( 'sliders', $args);
}