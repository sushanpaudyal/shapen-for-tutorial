<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shapen' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '~#f*v),PvH|Mck^`Hjv8e|zC,Fr+$.oDvSPdHzX0N8) #^+4 sE)W2w/[Rose(&K');
 define('SECURE_AUTH_KEY',  'H&IKRKg;SNWOU=jOHj|XvK5# &Q(o8iX<,M70j3(,SIg!k!|mC%Ve^jwNHT/>9T.');
 define('LOGGED_IN_KEY',    ',soxn&8nxAw+E,%~OpQi1M~e,V3?zND6fhL KA]]m`.Y]OK)BdwgF^&8!GXLDF4`');
 define('NONCE_KEY',        'jC2U3!>^rI+1|7ivc_e:0!se`)ef>#J+)dX-4FHHPS:U3)17XlGaj1NA{GiMn^Qu');
 define('AUTH_SALT',        '08bHxk&+$!4tc=yLpTUCK{Dt:|$E{~`[Mduk9xV3_/TVu}~2));NgD&8~xIQ$fcq');
 define('SECURE_AUTH_SALT', 'S+A2bA7/N1r2*wLldlqC#CCA=wtj%U1h=tLqB`T*Ga&+{nPQv]~!&~+P7Z;?^e)#');
 define('LOGGED_IN_SALT',   ']n93y?/6o9V%9XPK]&4;5T0qUAz+-=+|7Y< |2VYB)B=&UxFYE+fEh <AoX2+Z&d');
 define('NONCE_SALT',       'z+Az|7,dZC?)|;D`bQ+V@lIrrBa,-OE:OYiQ(7rk|DL+}TpR+=B8xzN/,Z Mo9pS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
